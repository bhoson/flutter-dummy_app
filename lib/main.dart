import 'package:flutter/material.dart';
import 'package:english_words/english_words.dart';

// Sample - 1

/*
void main()
{
  runApp(MaterialApp(
    title: 'Welcome to Flutter', 
    home: Scaffold(
      appBar: AppBar(
        title: Text('Flutter FTW!'),
      ),
    body: Center(
      child: Text('Yolo'),
      ),
  ),
  )
  );
}
*/




/*
// Sample - 2

void main() => runApp(MyApp());

class MyApp extends StatelessWidget
{
  final wordPair =WordPair.random();
  @override
  Widget build(BuildContext context)
  {
    return MaterialApp(
      title: 'Welcome to Flutter',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Welcome to Flutter'),
        ),
        body: Center(
          child: Text(wordPair.asPascalCase),
        ),
      ),);
  }
}
*/


/*
// Sample - 3


void main() => runApp(MyApp());

class RandomWords extends StatefulWidget
{
  @override
  RandomWordsState createState() => new RandomWordsState();
}


class RandomWordsState extends State<RandomWords>
{
  @override
  Widget build(BuildContext context)
  {
    final wordPair =WordPair.random();
    return Text(wordPair.asPascalCase);
  }
}



class MyApp extends StatelessWidget {
  //final wordPair =WordPair.random();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Welcome to Flutter',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Welcome to Flutter'),
        ),
        body: Center(
          child: RandomWords(),
          //child: Text(wordPair.asString),
        ),
      ),
    );
  }
}
*/